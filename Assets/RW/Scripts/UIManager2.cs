﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager2 : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene("CreateScene");
    }

    public void StartGame3()
    {
        SceneManager.LoadScene("Continue3");
    }

    public void StartGame4()
    {
        SceneManager.LoadScene("Continue4");
    }

    public void StartGame5()
    {
        SceneManager.LoadScene("Continue5");
    }

    public void StartGame6()
    {
        SceneManager.LoadScene("Continue6");
    }


    public void StartGame7()
    {
        SceneManager.LoadScene("Continue7");
    }

    public void StartGame8()
    {
        SceneManager.LoadScene("Continue8");
    }

    public void StartGame9()
    {
        SceneManager.LoadScene("Continue9");
    }

    public void StartGame10()
    {
        SceneManager.LoadScene("Continue10");
    }

    public void StartGame11()
    {
        SceneManager.LoadScene("Continue11");
    }

    public void StartGame12()
    {
        SceneManager.LoadScene("Continue12");
    }


    public GameObject Panel;

    public void OpenPanel()
    {
        if (Panel != null)
        {
            bool isActive = Panel.activeSelf;

            Panel.SetActive(!isActive);
        }
    }

}

